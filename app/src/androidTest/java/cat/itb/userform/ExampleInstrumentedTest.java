package cat.itb.userform;


import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActiveTestRule = new ActivityTestRule<>(MainActivity.class);

        @Test
        public void Login() {

            onView(withId(R.id.loginButton))
                    .check(matches(isDisplayed())) //Comprova que vegi el botó
                    .perform(click()); //Fa click al botó

            onView(withId(R.id.loginLayout))
                    .check(matches(isDisplayed()));

            onView(withId(R.id.usernameInput))
                    .check(matches(isDisplayed()))
                    .perform(typeText("Ramon"),closeSoftKeyboard());

            onView(withId(R.id.passwordInput))
                    .check(matches(isDisplayed()))
                    .perform(typeText("3872te"),closeSoftKeyboard());

            onView(withId(R.id.loginButton))
                    .check(matches(isDisplayed()))
                    .perform(click());

        }

    @Test
    public void RegistreIncorrecte(){
        onView(withId(R.id.registerButton))
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.usernameInput))
                .check(matches(isDisplayed()))
                .perform(typeText("Kyrie"),closeSoftKeyboard());

        onView(withId(R.id.passwordInput))
                .check(matches(isDisplayed()))
                .perform(typeText("ababshs3"),closeSoftKeyboard());

        onView(withId(R.id.confirmPasswordInput))
                .perform(scrollTo(),typeText("fh43a"),closeSoftKeyboard());

        onView(withId(R.id.emailInput))
                .check(matches(isDisplayed()))
                .perform(typeText("irwin345@gmail.com"),closeSoftKeyboard());

        onView(withId(R.id.registerButton))
                .perform(scrollTo(),click());

    }

    @Test
    public void LoginIncorrecte(){
        onView(withId(R.id.loginButton))
                        .check(matches(isDisplayed())) //Comprova que vegi el botó
                        .perform(click()); //Fa click al botó

        onView(withId(R.id.loginLayout))
                .check(matches(isDisplayed()));

        onView(withId(R.id.usernameInput))
                .check(matches(isDisplayed()));

        onView(withId(R.id.passwordInput))
                .check(matches(isDisplayed()));

        onView(withId(R.id.registerButton))
                .check(matches(isDisplayed()))
                .perform(click());
    }


    @Test
    public void NavLoginRegistre (){
        onView(withId(R.id.loginButton))
                .check(matches(isDisplayed())) //Comprova que vegi el botó
                .perform(click());
        onView(withId(R.id.registerButton))
                .check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.layoutRegister))
                .check(matches(isCompletelyDisplayed()));
    }
}