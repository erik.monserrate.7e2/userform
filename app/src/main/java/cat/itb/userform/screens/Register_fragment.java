package cat.itb.userform.screens;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.ui.main.MainViewModel;


public class Register_fragment extends Fragment {


    @BindView(R.id.birthDateInput)
    TextInputEditText birthDateInput;
    @BindView(R.id.genderSelector)
    TextInputEditText genderSelector;

    @BindView(R.id.usernameInput)
    TextInputEditText usernameInput;
    @BindView(R.id.passwordInput)
    TextInputEditText passwordInput;
    @BindView(R.id.confirmPasswordInput)
    TextInputEditText confirmPasswordInput;
    @BindView(R.id.emailInput)
    TextInputEditText emailInput;
    @BindView(R.id.registerButton)
    MaterialButton registerButton;


    private MainViewModel mViewModel;

    private String[] genders = {"Male", "Female"};


    public Register_fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        birthDateInput.setOnClickListener(this::showCalendar);
        genderSelector.setOnClickListener(this::showGender);

        registerButton.setOnClickListener(this::validar);
    }

    private void validar(View view) {
        if (validate()) {
            System.out.println("CORRECTO!");
        }
    }


    private void doOnDateSelected(Long aLong) {
        birthDateInput.setText(mViewModel.datePicker(aLong));
    }

    private void showCalendar(View view) {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText(R.string.birth_date);
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());
    }

    private void showGender(View view) {
        new MaterialAlertDialogBuilder(getContext())
                .setTitle(R.string.Select)
                .setItems(genders, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        genderSelector.setText(genders[i]);
                    }
                })
                .show();
    }

    private boolean validate() {
        boolean valid = true;
        ArrayList<TextInputEditText> obligatoris = new ArrayList<>();
        obligatoris.add(usernameInput);
        obligatoris.add(passwordInput);
        obligatoris.add(confirmPasswordInput);
        obligatoris.add(emailInput);

        for (TextInputEditText obligatori : obligatoris) {
            valid = validateNoEmpty(obligatori) && valid;
        }
        valid = validateEmail(emailInput) && valid;
        valid = validatePassword(confirmPasswordInput) && valid;
        return valid;
    }

    private boolean validateNoEmpty(TextInputEditText input) {
        if (input.getEditableText().toString().isEmpty()) {
            input.setError("Introdueix camp obligatori");
            scrollTo(input);
            return false;
        }
        return true;
    }

    private boolean validateEmail(TextInputEditText email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (!pattern.matcher(email.getEditableText().toString()).matches()) {
            email.setError("Adreça no vàlida");
            return false;
        }
        return true;
    }

    private boolean validatePassword(TextInputEditText password) {
        if (!password.getEditableText().toString().equals(passwordInput.getEditableText().toString())) {
            confirmPasswordInput.setError("Les contrassenya no coincideix !");
            return false;
        }
        return true;
    }

    private void scrollTo(TextInputEditText targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }


    @OnClick(R.id.loginButton)
    public void toLogin(View view) {
        Navigation.findNavController(view).navigate(R.id.action_register_fragment_to_login_fragment);
    }
}
