package cat.itb.userform.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class MainFragment extends Fragment {

    @BindView(R.id.loginButton)
    MaterialButton loginButton;
    @BindView(R.id.registerButton)
    MaterialButton registerButton;
    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

    }


    @OnClick({R.id.loginButton, R.id.registerButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                Navigation.findNavController(getView()).navigate(R.id.action_mainFragment_to_login_fragment);
                break;
            case R.id.registerButton:
                Navigation.findNavController(getView()).navigate(R.id.action_mainFragment_to_register_fragment);
                break;
        }
    }
}
