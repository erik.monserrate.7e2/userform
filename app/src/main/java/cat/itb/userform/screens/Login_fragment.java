package cat.itb.userform.screens;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.ui.main.MainViewModel;


public class Login_fragment extends Fragment {

    MainViewModel mViewModel;
    @BindView(R.id.usernameInput)
    TextInputEditText usernameInput;
    @BindView(R.id.passwordInput)
    TextInputEditText passwordInput;
    @BindView(R.id.loginButton)
    MaterialButton loginButton;
    @BindView(R.id.registerButton)
    MaterialButton registerButton;

    private Dialog dialog;

    public Login_fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);


        loginButton.setOnClickListener(this::log);


    }

    private void log(View view) {
        if (isValid()) {
            System.out.println("CORRECTO");
            Navigation.findNavController(view).navigate(R.id.action_login_fragment_to_logged);
        }
    }

    private boolean isValid() {
        boolean valid = true;
        valid = validateNoEmpty(usernameInput) && valid;
        valid = validateNoEmpty(passwordInput) && valid;

        return valid;
    }

    private boolean validateNoEmpty(TextInputEditText input) {
        if (input.getEditableText().toString().isEmpty()) {
            input.setError("Introdueix camp obligatori");
            scrollTo(input);
            return false;
        }
        return true;

    }

    private void scrollTo(TextInputEditText targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

    @OnClick(R.id.registerButton)
    public void toRegister(View view) {
        Navigation.findNavController(view).navigate(R.id.action_login_fragment_to_register_fragment);
    }
}
